const longestSubstring = {
    index: 0,
    length: 0,
};
const x = (array: number[]) => {
    if (!array.includes(0)) {
        return 0
    }

    for (let i = 0; i < array.length; i++) {
        const newArray = [...array]
        newArray[i] = 1
        const x = newArray.join('').split('0')

        for (let index = 0; index < x.length; index++) {
            const item = x[index]
            if (item.length >= longestSubstring.length) {
                longestSubstring.index = i
                longestSubstring.length = item.length
            }
        }

    }
    console.log(longestSubstring.index);
}
